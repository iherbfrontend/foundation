# [iHerb FE Foundation](https://fe.iherb.com)


Foundation library designed to help developers create the best experience customers of Iherb. Visit the [core-packages]() to learn more.


### Installation

Run the following command using [npm](https://www.npmjs.com/):

### Usage


## Using the CSS components

If React doesn’t make sense for your application, you can use a CSS-only version of our components. This includes all the styles you need for every component in the library, but you’ll be responsible for writing the correct markup and updating classes and DOM attributes in response to user events.

### Usage


## Examples

We have created example applications to document some of the ways you could include foundation in one of your own applications. Each of these examples includes further documentation on how to install dependencies and run the app:

- [create-react-app example]()
- [create-react-app with TypeScript and react-testing example]()
- [Webpack example]()
- [CSS-only example]()
- [next.js example]()

## Development

We use Storybook to create a simple, hot-reloading playground for development on these components. You can edit the `playground/Playground.tsx` file to import the components you are working on, and run `yarn dev` in order to start the development server. Please do not commit your work on the playground so that it remains pristine for other developers to work on.

### Testing on mobile or a virtual machine


### Testing in a consuming project

#### Manual visual regression testing

To start a server for manually viewing the visual regression testing examples, run `yarn run dev`.

## Methodology

We set out to make our components easy to use. Each of our components has a well-documented (and fully typed) public interface with strong, consistently-applied conventions. This way, developers don’t need to worry about the underlying implementation. Instead, they can focus on creating amazing merchant experiences.

We ensure that our components are made for everyone. They meet accessibility standards and are responsive to any screen or device. We also put a lot of effort into optimizing the performance of the components, so everyone can build inclusive experiences that work.

## Contributing

Pull requests are welcome. See the [contribution guidelines]() for more information.

## Licenses